# Makefile de exemplo (Manual do GNU Make)
     
CFLAGS = -Wall -g  # flags de compilacao
LDFLAGS = -lm

CC = gcc

# arquivos-objeto
	objects = rle.o
     
rle: rle.o
	$(CC) -o rle rle.o $(LDFLAGS)

rle.o: rle.c
	$(CC) -c $(CFLAGS) rle.c

clean:
	rm -f $(objects) rle

purge: clean
	-rm -f beale ArquivoOriginal ArquivoComprimido ArquivoDescomprimido


 