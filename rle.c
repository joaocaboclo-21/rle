#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

typedef enum { NOP, ENCODE, DECODE } Modo_t;

#define BUFFER_SIZE 10

// PROTÓTIPOS

// Função que comprime 'fIn', gravando o resultado da compressão em 'fOut'
void Encode (char *fIN, char *fOut);

// Função que descomprime 'fIn', gravando o resultado da descompressão em 'fOut'
void Decode (char *fIN, char *fOut);

// Função  ser  usada  no  programa para  chamar  Encode()  ou  Decode()
void rle (char *fIN, char *fOut, void (*func) (char*, char*));


// PROGRAMA PRINCIPAL
int main(int argc, char *argv[])
{

  int opt;
  char *input = NULL;
  char *output = NULL;
  Modo_t modo = NOP;
  
  while ((opt = getopt(argc, argv, "dei:o:")) != -1) {
    switch (opt) {
    case 'i':
      input = strdup(optarg);
      break;
    case 'o':
      output = strdup(optarg);
      break;
    case 'e':
      modo |= ENCODE;
      break;
    case 'd':
      modo |= DECODE;
      break;
    default:
      fprintf(stderr, "Forma de uso: ./rle [-e | -d] -i <arq_in> -o <arq_out>\n");
      exit(1);
    }		
  }

  // Erro se:
  //  * não forneceu os dois arquivos
  //  * não forneceu uma das opções '-e' ou '-d',
  //  * forneceu as opções '-e' e '-d' na mesma linha de comando
  if ( ! input || ! output || modo == NOP || modo & (modo-1) ) {
    fprintf(stderr, "Forma de uso: ./rle [-e | -d] -i <arq_in> -o <arq_out>\n");
    exit(2);
  }
  
  /* COMPLEMENTE O CÓDIGO PARA DESENVOLVER O PROGRAMA PRINCIPAL A PARTIR DESTE PONTO */
  if (modo == ENCODE)
    rle(input, output, Encode);
  else if (modo == DECODE)
    rle(input, output, Decode);

  return 0 ;

}


/* DEFINA ABAIXO O CÓDIGO DAS FUNÇÕES Encode(), Decode() e rle()  */

// Função ser usada no programa principal para chamar Encode() ou Decode()
void rle (char *fIN, char *fOut, void (*func) (char*, char*))
{
  func(fIN, fOut);
}



// Função que comprime 'fIn', gravando o resultado da compressão em 'fOut'
void Encode (char *fIN, char *fOut)
{
  FILE *In = fopen(fIN, "r");
  FILE *Out = fopen(fOut, "w");

  if (!In || !Out){
    perror("Erro ao abrir arquivo");
    exit(1);
  }

  char value, prox_value;
  unsigned int cont = 1;

  fread(&value, sizeof(char), 1, In);
  while (!feof(In)){

      if (cont == 1){
        fwrite(&value, sizeof(char), 1, Out);
      }

      fread(&prox_value, sizeof(char), 1, In);

      if (value == prox_value){
        cont++;
        value = prox_value;
      }
      else{
        fwrite(&cont, sizeof(unsigned int), 1, Out);
        cont = 1;
        value = prox_value;
      }
  }

}



// Função que descomprime 'fIn', gravando o resultado da descompressão em 'fOut'
void Decode (char *fIN, char *fOut)
{
  FILE *In = fopen(fIN, "r");
  FILE *Out = fopen(fOut, "w");

  if (!In || !Out){
    perror("Erro ao abrir arquivo");
    exit(1);
  }

  char letra;
  unsigned int value, i;

  fread(&letra, sizeof(char), 1, In);
  fread(&value, sizeof(unsigned int), 1, In);
  while (!feof(In)){
    
    for (i = 0; i < value; i++)
      fwrite(&letra, sizeof(char), 1, Out);

    fread(&letra, sizeof(char), 1, In);
    fread(&value, sizeof(unsigned int), 1, In);
  }

}

